package com.comteco.spring.aprendiendo.spring5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAprendiendoSpring5Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringAprendiendoSpring5Application.class, args);
	}

}
